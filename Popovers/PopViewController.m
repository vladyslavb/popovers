//
//  PopViewController.m
//  Popovers
//
//  Created by Vladyslav Bedro on 6/12/18.
//  Copyright © 2018 Vladyslav Bedro. All rights reserved.
//

#import "PopViewController.h"

@interface PopViewController ()

@end

@implementation PopViewController


#pragma mark - Life cycle -

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // add touch recogniser to dismiss this controller
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissMe)];
    [self.view addGestureRecognizer:tap];
}


#pragma mark - Memory managment -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Internal methods -

- (void)dismissMe {
    
    NSLog(@"Popover was dismissed with internal tap");
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
